# UpdateNotifier Script - README #
---

### Overview ###

The **UpdateNotifier** script (Bash) can be executed by a cronjob and checks the actual version of a [**DietPi installation**](https://dietpi.com). If there is an update available, it will send an email notification to the user.


### Setup ###

* Install the software packages **sendemail**, **libnet-ssleay-perl** and **libio-socket-ssl-perl**.
* You can use the command: **apt-get install sendemail libnet-ssleay-perl libio-socket-ssl-perl**
* Copy the backup script **update_notifier.sh** to your computer.
* Make the backup script executable: **chmod +x update_notifier.sh**.
* Edit the configuration part of the **update_notifier.sh** script.
* Start the script from the command prompt: **./update_notifier.sh**
* Check if you get an email notification if an update is available.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **UpdateNotifier** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
