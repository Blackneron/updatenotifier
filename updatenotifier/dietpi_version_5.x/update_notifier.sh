#!/bin/bash


########################################################################
#                                                                      #
#             D I E T P I   U P D A T E   N O T I F I E R              #
#                                                                      #
#     SENDS AN EMAIL IF NEW UPDATES FOR DIETPI HOSTS ARE AVAILABLE     #
#                                                                      #
#                      Copyright 2017 by PB-Soft                       #
#                                                                      #
#                           www.pb-soft.com                            #
#                                                                      #
#                             Version 1.2                              #
#                                                                      #
########################################################################
#
#     IMPORTANT: This version is used for versions of DietPi < 6.0
#
########################################################################
#
# A part of this code was used from the DietPi update file which is
# used to update the DietPi software. The great DietPi software was
# created by Daniel Knight. For more information check the links:
#
#   Website: https://dietpi.com
#
#   Email:   daniel.knight@dietpi.com
#
# Thanks a lot to Daniel Knight for the great DietPi distro!!
#
########################################################################
#
# Check that the following software packages are installed on your
# system if you want to send emails:
#
#  - sendemail (lightweight, command line SMTP email client)
#
#  - libnet-ssleay-perl (perl module to call Secure Sockets Layer (SSL)
#                       functions of the SSLeay library)
#
#  - libio-socket-ssl-perl (perl module that uses SSL to encrypt data)
#
# ======================================================================
#
# You can use the following commands to install the packages:
#
#   apt-get install sendemail
#
#   apt-get install libnet-ssleay-perl
#
#   apt-get install libio-socket-ssl-perl
#
# ======================================================================
#
# Check the following link for information about the sendEmail tool:
#
#   http://caspian.dotconf.net/menu/Software/SendEmail/
#
# ======================================================================


########################################################################
########################################################################
##############   C O N F I G U R A T I O N   B E G I N   ###############
########################################################################
########################################################################


# ======================================================================
# Specify the connection details for the SMTP host.
# ======================================================================
SUSERNAME="john@gmail.com"
SPASSWORD="pass"
SHOSTNAME="smtp.gmail.com"
SPORT="587"
STLS="yes"


# ======================================================================
# Specify the email sender.
# ======================================================================
SCRIPTHOST="$(hostname)"
ESENDER="admin@$SCRIPTHOST"


# ======================================================================
# Specify the email receiver(s). Multiple receivers may be specified by
# separating them by either a white space, comma, or semi-colon like:
#
#  ERECEIVER="john@example.com brad@domain.com"
#
# ======================================================================
ERECEIVER="brad@domain.com"


# ======================================================================
# Specify if email notifications should be sent (yes/no).
# ======================================================================
NOTIFICATIONS="yes"


# ======================================================================
# Specify the URLs to get the version of the mirror servers.
# ======================================================================
GITFORKOWNER=$(cat /DietPi/dietpi.txt | grep -m1 '^gitforkowner=' | sed 's/.*=//')
GITBRANCH=$(cat /DietPi/dietpi.txt | grep -m1 '^gitbranch=' | sed 's/.*=//')
URL_MIRROR_SERVERVERSION=(
  "http://dietpi.com/downloads/dietpi-update_mirror/$GITBRANCH/server_version"
  "https://raw.githubusercontent.com/${GITFORKOWNER:-Fourdee}/DietPi/$GITBRANCH/dietpi/server_version"
)


########################################################################
########################################################################
################   C O N F I G U R A T I O N   E N D   #################
########################################################################
########################################################################


# ======================================================================
# Get necessary information from existing DietPi files.
# ======================================================================
DIETPIUPDATE_VERSION_CURRENT=$(cat /DietPi/dietpi/dietpi-update | grep -m50 'DIETPIUPDATE_VERSION_CURRENT=' | cut -d'=' -f2 | cut -d' ' -f1)
VERSION_CURRENT=$(cat /DietPi/dietpi/.version)


# ======================================================================
# Initialize some variables.
# ======================================================================
FILEPATH_TEMP="/tmp/dietpi-update"
DIETPIUPDATE_VERSION_REQUIRED=0
VERSION_SERVER=0
EMESSAGE=""


# ======================================================================
# Check if the temporary directory does not exist.
# ======================================================================
if [ ! -d $FILEPATH_TEMP ]; then


  # ====================================================================
  # Create the temporary directory.
  # ====================================================================
  mkdir -p $FILEPATH_TEMP
fi


# ======================================================================
# Loop through the URLs of the mirror servers.
# ======================================================================
for ((i=0; i<${#URL_MIRROR_SERVERVERSION[@]}; i++))
do


  # ====================================================================
  # Get the server version file from the mirror servers.
  # ====================================================================
	curl -s -k -L ${URL_MIRROR_SERVERVERSION[$i]} > "$FILEPATH_TEMP"/server_version


  # ====================================================================
  # Check if the server version file was downloaded successfully.
  # ====================================================================
	if (( $? == 0 )); then


    # ==================================================================
    # Get the version of the server.
    # ==================================================================
		VERSION_SERVER=$(sed -n 1p "$FILEPATH_TEMP"/server_version)


    # ==================================================================
    # Get the version of the required update version.
    # ==================================================================
		DIETPIUPDATE_VERSION_REQUIRED=$(sed -n 2p "$FILEPATH_TEMP"/server_version)


    # ==================================================================
    # Check if the version number of the server is valid.
    # ==================================================================
		if [[ $VERSION_SERVER =~ ^-?[0-9]+$ ]]; then


      # ================================================================
      # Check if the current DietPi update version is old.
      # ================================================================
      if (( $DIETPIUPDATE_VERSION_CURRENT < $DIETPIUPDATE_VERSION_REQUIRED )); then


        # ==============================================================
        # Create messages to inform the user.
        # ==============================================================
        EMESSAGE="The DietPi update for host '$SCRIPTHOST' requires a new image!\nCurrent version: $VERSION_CURRENT\nNew version: $VERSION_SERVER"
        echo "The DietPi update requires a new image! $VERSION_CURRENT ==> $VERSION_SERVER"


      # ================================================================
      # Check if the current DietPi version needs an update.
      # ================================================================
      elif (( $VERSION_CURRENT < $VERSION_SERVER )); then


        # ==============================================================
        # Create messages to inform the user.
        # ==============================================================
        EMESSAGE="DietPi update for host '$SCRIPTHOST' available!\nCurrent version: $VERSION_CURRENT\nNew version: $VERSION_SERVER"
        echo "DietPi update available: $VERSION_CURRENT ==> $VERSION_SERVER"


      # ================================================================
      # There is no DietPi update available.
      # ================================================================
      else


        # ==============================================================
        # Display an information message.
        # ==============================================================
        echo "No DietPi update available..."
      fi


      # ================================================================
      # Check if a notification message has to be sent by email.
      # ================================================================
      if [[ "$EMESSAGE" != "" && "$NOTIFICATIONS" == "yes" ]]; then


        # ==============================================================
        # Create the email subject.
        # ==============================================================
        ESUBJECT="DietPi update for host '$SCRIPTHOST' available!"


        # ==============================================================
        # Send the email.
        # ==============================================================
        SENDEMAIL="$(which sendEmail)"
        $SENDEMAIL -o tls=$STLS -f $ESENDER -t $ERECEIVER -s $SHOSTNAME:$SPORT -xu $SUSERNAME -xp $SPASSWORD -u "$ESUBJECT" -m "$EMESSAGE" -o message-charset=utf-8
      fi


    # ==================================================================
    # The version number of the server is not valid (no connection).
    # ==================================================================
    else


      # ================================================================
      # Display an error message.
      # ================================================================
      echo "Unable to access the DietPi update servers!"
      exit
		fi
    exit
	fi
done
