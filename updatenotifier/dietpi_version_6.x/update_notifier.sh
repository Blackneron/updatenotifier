#!/bin/bash

########################################################################
#                                                                      #
#             D I E T P I   U P D A T E   N O T I F I E R              #
#                                                                      #
#     SENDS AN EMAIL IF NEW UPDATES FOR DIETPI HOSTS ARE AVAILABLE     #
#                                                                      #
#                      Copyright 2018 by PB-Soft                       #
#                                                                      #
#                           www.pb-soft.com                            #
#                                                                      #
#                             Version 1.4                              #
#                                                                      #
########################################################################
#
#     IMPORTANT: This version is used for versions of DietPi >= 6.0
#
########################################################################
#
# A part of this code was used from the DietPi update file which is
# used to update the DietPi software. The great DietPi software was
# created by Daniel Knight. For more information check the links:
#
#   Website: https://dietpi.com
#
#   Email:   daniel.knight@dietpi.com
#
# Thanks a lot to Daniel Knight for the great DietPi distro!!
#
########################################################################
#
# Check that the following software packages are installed on your
# system if you want to send emails:
#
#   sendemail (lightweight, command line SMTP email client)
#
#   libnet-ssleay-perl (perl module to call Secure Sockets Layer (SSL)
#                       functions of the SSLeay library)
#
#   libio-socket-ssl-perl (perl module that uses SSL to encrypt data)
#
# ======================================================================
#
# You can use the following commands to install the packages:
#
#   apt-get install sendemail
#
#   apt-get install libnet-ssleay-perl
#
#   apt-get install libio-socket-ssl-perl
#
# ======================================================================
#
# Check the following link for information about the sendEmail tool:
#
#   http://caspian.dotconf.net/menu/Software/SendEmail/
#
# ======================================================================


########################################################################
########################################################################
##############   C O N F I G U R A T I O N   B E G I N   ###############
########################################################################
########################################################################


# ======================================================================
# Specify the connection details for the SMTP host.
# ======================================================================
SUSERNAME="john@gmail.com"
SPASSWORD="pass"
SHOSTNAME="smtp.gmail.com"
SPORT="587"
STLS="yes"


# ======================================================================
# Specify the email sender.
# ======================================================================
SCRIPTHOST="$(hostname)"
ESENDER="admin@$SCRIPTHOST"


# ======================================================================
# Specify the email receiver(s). Multiple receivers may be specified by
# separating them by either a white space, comma, or semi-colon like:
#
#  ERECEIVER="john@example.com brad@domain.com"
#
# ======================================================================
ERECEIVER="brad@domain.com"


# ======================================================================
# Specify if email notifications should be sent (yes/no).
# ======================================================================
NOTIFICATIONS="yes"


########################################################################
########################################################################
################   C O N F I G U R A T I O N   E N D   #################
########################################################################
########################################################################

# Clear the screen.
clear

# Display an information message.
echo
echo "========================================================================"
echo "  Check for DietPi updates on host '$SCRIPTHOST'"
echo "========================================================================"
echo

# Initialize some necessary variables.
COREVERSION_SERVER=0
SUBVERSION_SERVER=0
SERVER_ONLINE=0
NEW_UPDATE=0
NEW_IMAGE=0
EMESSAGE=""

# Get the gitbranch name from the file 'dietpi.txt'.
GITBRANCH=$(grep -m1 '^DEV_GITBRANCH=' /DietPi/dietpi.txt | sed 's/.*=//')

# Get the name of the git fork owner from the file 'dietpi.txt'.
GITFORKOWNER=$(grep -m1 '^DEV_GITOWNER=' /DietPi/dietpi.txt | sed 's/.*=//')

# Specify the URL for the mirror servers used to get the server versions.
URL_MIRROR_SERVERVERSION=(
  "http://dietpi.com/downloads/dietpi-update_mirror/$GITBRANCH/server_version-6"
  "https://raw.githubusercontent.com/${GITFORKOWNER:-Fourdee}/DietPi/$GITBRANCH/dietpi/server_version-6"
)

# Specify the actual core version of the 'dietpi-update' tool.
DIETPIUPDATE_COREVERSION_CURRENT=6

# Get the actual core client version from the '.version' file.
COREVERSION_CURRENT=$(sed -n 1p /DietPi/dietpi/.version)

# Get the actual client subversion from the '.version' file.
SUBVERSION_CURRENT=$(sed -n 2p /DietPi/dietpi/.version)


# ======================================================================
#                  G E T   S E R V E R   V E R S I O N
# ======================================================================

# Loop through the server URLs to get the actual server version.
for ((i=0; i<${#URL_MIRROR_SERVERVERSION[@]}; i++))
do

  # Display information about the contacted mirror.
  echo "Checking mirror: ${URL_MIRROR_SERVERVERSION[$i]}..."

  # Get the actual server versions from the Github repository.
  SERVER_VERSIONS=$(curl -sS -k -L ${URL_MIRROR_SERVERVERSION[$i]})

  # Specify the actual core server version.
  COREVERSION_SERVER=$(echo $SERVER_VERSIONS | awk '{ print $1}')

  # Specify the actual server subversion.
  SUBVERSION_SERVER=$(echo $SERVER_VERSIONS | awk '{ print $2}')

  # Check if server version is a valid interger.
  if [[ $SUBVERSION_SERVER =~ ^-?[0-9]+$ ]]; then

    # Enable the server online flag.
    SERVER_ONLINE=1

    # Display information about the used update server.
    echo "Using update server: ${URL_MIRROR_SERVERVERSION[$i]}"
    echo

    # Break the loop.
    break

  # The server version is not valid.
  else

    # Display an error message.
    echo "Invalid server version and/or update file unavailable!"
  fi

# End of the loop to get the actual server version.
done


# ======================================================================
#                   C H E C K   F O R   U P D A T E S
# ======================================================================

# Check if an update is available.
if (( $SERVER_ONLINE )); then

  # Check if the update requires a new image.
  if (( $DIETPIUPDATE_COREVERSION_CURRENT < $COREVERSION_SERVER )); then

    # Enable the new image flag.
    NEW_IMAGE=1

  # There is an update available.
  elif (( $SUBVERSION_CURRENT < $SUBVERSION_SERVER )); then

    # Enable the new update flag.
    NEW_UPDATE=1
  fi
fi


# ======================================================================
#                        C H E C K   R E S U L T
# ======================================================================

# Specify the version output.
VERSION_OUTPUT="\nCurrent Version: $COREVERSION_CURRENT.$SUBVERSION_CURRENT\n"
VERSION_OUTPUT+="New Version:     $COREVERSION_SERVER.$SUBVERSION_SERVER\n"

# Check if the update server is not online.
if (( $SERVER_ONLINE == 0 )); then

  # Display an error message.
  echo "Cannot connect to the Github server!"

# Check if the update requires a new DietPi image.
elif (( $NEW_IMAGE == 1 )); then

  # Display the result.
  echo "There are updates available! => A new image is required!"
  echo -e $VERSION_OUTPUT

  # Specify the email message.
  EMESSAGE="The DietPi update for host '$SCRIPTHOST' requires a new image!\nCurrent version: $COREVERSION_CURRENT.$SUBVERSION_CURRENT\nNew version: $COREVERSION_SERVER.$SUBVERSION_SERVER"

# Check if there is a update available.
elif (( $NEW_UPDATE == 1 )); then

  # Display the result.
  echo "There are updates available!"
  echo -e $VERSION_OUTPUT

  # Specify the email message.
  EMESSAGE="DietPi update for host '$SCRIPTHOST' available!\nCurrent version: $COREVERSION_CURRENT.$SUBVERSION_CURRENT\nNew version: $COREVERSION_SERVER.$SUBVERSION_SERVER"

# There are no updates available.
else

  # Display the result.
  echo "There are no updates available!"
  echo -e $VERSION_OUTPUT
fi


# ======================================================================
#                         S E N D   E - M A I L
# ======================================================================

# Check if a notification message has to be sent by email.
if [[ "$EMESSAGE" != "" && "$NOTIFICATIONS" == "yes" ]]; then

  # Specify the email subject.
  ESUBJECT="DietPi update for host '$SCRIPTHOST' available!"

  # Specify the path to the email executable.
  SENDEMAIL="$(which sendEmail)"

  # Send the email.
  $SENDEMAIL -o tls=$STLS -f $ESENDER -t $ERECEIVER -s $SHOSTNAME:$SPORT -xu $SUSERNAME -xp $SPASSWORD -u "$ESUBJECT" -m "$EMESSAGE" -o message-charset=utf-8

  # Add some space.
  echo
fi
